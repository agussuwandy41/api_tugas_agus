<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();
        return response()->json($contacts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contacts = Contact::create($request->all());
        
        if ($contacts){
            return response()->json([
                'status' => 'success',
                'message' => 'Data berhasil ditambahkan',
                'data' => $contacts
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Data gagal ditambahkan'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contacts = Contact::where('id',$id)->first();
        return response()->json($contacts);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contacts = Contact::where('id',$id)->first();

        if ($contacts){
            $contacts->update($request->all());
            return response()->json([
                'status' => 'success',
                'message' => 'Data berhasil diubah',
                'data' => $contacts
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Data gagal diubah'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contacts = Contact::where('id',$id)->first();

        if ($contacts){
            $contacts->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'Data berhasil dihapus',
                'data' => $contacts
            ]);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Data gagal dihapus'
            ]);
        }
    }
}